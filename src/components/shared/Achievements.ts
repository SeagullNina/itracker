export interface IAchievement {
  id: number
  title: string
  description: string
  src: string
}

export const Achievements = [
  {
    id: 1,
    title: 'Дисциплинированный солдат: ',
    description: 'не пропускать выполнение заданий 3 дня подряд',
    src: '../soldier.png',
  },
  {
    id: 2,
    title: 'Большая нагрузка: ',
    description: 'поставить на день не менее 5 заданий',
    src: '../gym.png',
  },
  {
    id: 3,
    title: 'Путь к успеху: ',
    description: 'не пропускать выполнение заданий одну неделю',
    src: '../success.png',
  },
  {
    id: 4,
    title: 'Знай своих героев: ',
    description: 'заполнить информацию в профиле и выбрать аватарку',
    src: '../hero.png',
  },
  {
    id: 5,
    title: 'Ежедневная рутина: ',
    description:
      'перевести все задания из "ожидают выполнения" в "выполненные" ',
    src: '../employee.png',
  },
]

export const getAchievements = (achivements: number[]): IAchievement[] =>
  Achievements.filter((item: IAchievement) =>
    achivements.find(value => value === item.id)
  )
