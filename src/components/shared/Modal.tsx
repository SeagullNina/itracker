import React from 'react'
import styles from './Modal.module.scss'
import { ReactComponent as CloseIcon } from 'src/assets/icons/cross.svg'

const useOnClickOutside = (ref1: any, ref2: any, handler: any) => {
  React.useEffect(() => {
    const listener = (event: any) => {
      if (
        !ref1.current ||
        ref1.current.contains(event.target) ||
        !ref2.current ||
        ref2.current.contains(event.target)
      ) {
        return
      }
      handler(event)
    }
    document.addEventListener('mousedown', listener)
    document.addEventListener('touchstart', listener)
    return () => {
      document.removeEventListener('mousedown', listener)
      document.removeEventListener('touchstart', listener)
    }
  }, [ref1, ref2, handler])
}

const Modal = (props: any) => {
  const { label, opened, onClose, children} = props
  const ref = React.useRef(null)
  const ref2 = React.useRef(null)

  useOnClickOutside(ref, ref2, onClose)

  return (
    opened && (
      <div className={styles.modal}>
        <div ref={ref} className={styles.header}>
          {label}
          <CloseIcon className={styles.closeModal} onClick={onClose} />
        </div>
        <div
          ref={ref2}
          className={styles.content}
        >
          {children}
        </div>
      </div>
    )
  )
}

export default Modal
