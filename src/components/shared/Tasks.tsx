import { IEvent } from 'src/classes/models/IEvent'
import Task from 'src/components/Profile/Task'
import styles from 'src/components/shared/Tasks.module.scss'
import React from 'react'
import Modal from 'src/components/shared/Modal'
import DayOfWeek from './DayOfWeek'
import nanoid from 'nanoid'
import { IDuration } from '../../classes/models/IDuration'
import EventsApi from '../../classes/services/api/EventsApi'

interface IProps {
  profile: boolean
  setEvents: (e: any[]) => void
  events?: IEvent[]
  tasks?: IEvent[]
  setTask?: (e: any) => void
  currentDate?: Date
}

export const IDayOfWeek: any = {
  Пн: 1,
  Вт: 2,
  Ср: 3,
  Чт: 4,
  Пт: 5,
  Сб: 6,
  Вс: 0,
}

const duration = [
  {
    id: 1,
    name: 'Неделя',
  },
  {
    id: 2,
    name: 'Месяц',
  },
  {
    id: 3,
    name: '3 месяца',
  },
]

const Tasks = (props: IProps) => {
  const profile = props.profile
  const [showModal, setShowModal] = React.useState(false)
  const [habit, setHabit] = React.useState<string>('')
  const [tasks, setTasks] = React.useState()
  const [days, setDays] = React.useState<number[]>([])
  const [currentTask, setCurrentTask] = React.useState(0)
  const [currentDuration, setCurrentDuration] = React.useState<IDuration>(2)
  const currentDate = props.currentDate

  React.useEffect(() => {
    setTasks(props.tasks || props.events)
  }, [props.events, props.tasks])

  const deleteTask = (_id: string) => {
    let newTasks = tasks.filter((i: IEvent) => _id !== i._id)
    if (props.events)
      props.setEvents(props.events.filter((i: IEvent) => _id !== i._id))
    setTasks(newTasks)
  }
  const addTask = (hab: string) => {
    props.events &&
      props.setEvents([
        ...props.events,
        {
          _id: nanoid(8),
          title: hab,
          start: currentDate ? currentDate : '',
        } as IEvent,
      ])
  }
  return (
    <>
      {tasks?.map((task: IEvent, index: number) => (
        <Task
          active={index === currentTask}
          calendar={!!props.tasks}
          onClick={() => {
            setCurrentTask(index)
            return props.setTask && props.setTask(task)
          }}
          key={index}
          task={task}
          deleteTask={task => deleteTask(task)}
        />
      ))}
      <input
        type="button"
        className={styles.add}
        onClick={() => {
          setShowModal(true)
        }}
        value="+ добавить задание"
      />
      <Modal
        label="Добавление задания"
        opened={showModal}
        onClose={() => {
          setShowModal(false)
          setHabit('')
          localStorage.setItem('event', JSON.stringify(''))
        }}
      >
        <div className={styles.name}>
          <span>Название: </span>
          <input
            type="text"
            className={styles.inputModal}
            onChange={event => {
              setHabit(event.target.value)
            }}
          />
        </div>
        {profile && (
          <>
          </>
        )}
        <div
          className={styles.save}
          onClick={() => {
            setShowModal(false)
            if (habit.length !== 0) addTask(habit)
            setHabit('')
            const userId = JSON.parse(localStorage.getItem('user') as string)
              ._id
            EventsApi.createEvent({
              userId,
              title: habit,
              duration: currentDuration,
              frequency: days,
              start: currentDate ? currentDate.toJSON() : ''
            }).then((result: any) => {
              props.events &&
              props.setEvents([
                ...props.events,
                {
                  ...result.data
                } as IEvent,
              ])
            })
          }}
        >
          Сохранить
        </div>
      </Modal>
    </>
  )
}

export default Tasks
