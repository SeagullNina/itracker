import React from 'react'
import styles from './DayOfWeek.module.scss'
import { IDayOfWeek } from './Tasks'

interface IProps {
  days: number[]
  name: string
  setDays: (arg: number[]) => void
}

const DayOfWeek = (props: IProps) => {
  const { name, days, setDays } = props
  const [currentDay, setCurrentDay] = React.useState(false)

  return (
    <input
      type="button"
      className={currentDay ? styles.dayActive : styles.day}
      value={name}
      onClick={() => {
        setDays(
          [...days, IDayOfWeek[name]].filter((v, i, a) => a.indexOf(v) === i)
        )
        if (!currentDay) {
          setCurrentDay(true)
        } else {
          setCurrentDay(false)
        }
      }}
    />
  )
}

export default DayOfWeek
