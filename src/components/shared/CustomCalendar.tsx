import React from 'react'
import moment from 'moment'
import 'moment/locale/ru'
import styles from './CustomCalendar.module.scss'
import nanoid from 'nanoid'
import classNames from 'classnames'
import { IEvent } from 'src/classes/models/IEvent'

interface IProps {
  currentDate: Date
  setCurrentDate: (date: Date) => void
  selectedDate: Date
  setSelectedDate: (date: Date) => void
  width: number
  height: number
  events: IEvent[]
}

const CustomCalendar = (props: IProps) => {
  const {
    events,
    currentDate,
    selectedDate,
    setCurrentDate,
    setSelectedDate,
  } = props
  moment.locale('ru')

  const header = () => {
    const dateFormat = 'MMMM YYYY'
    return (
      <div
        className={classNames(styles.header, styles.row, styles['flex-middle'])}
      >
        <div className={classNames(styles.column, styles['col-start'])}>
          <div className={styles.icon} onClick={prevMonth}>
            <img src="../prev.png" alt="" />
          </div>
        </div>
        <div className={classNames(styles.column, styles['col-center'])}>
          <span>{moment(currentDate).format(dateFormat)}</span>
        </div>
        <div className={classNames(styles.column, styles['col-end'])}>
          <div className={styles.icon} onClick={nextMonth}>
            <img src="../next.png" alt="" />
          </div>
        </div>
      </div>
    )
  }

  const days = () => {
    const dateFormat = 'ddd'
    const days = []
    let startDate = moment(currentDate).startOf('week')
    for (let i = 0; i < 7; i++) {
      days.push(
        <div
          className={classNames(styles.column, styles['col-center'])}
          key={i}
        >
          {moment(startDate)
            .add(i, 'd')
            .format(dateFormat)}
        </div>
      )
    }
    return <div className={classNames(styles.days, styles.row)}>{days}</div>
  }

  const cells = () => {
    const monthStart = moment(currentDate).startOf('month')
    const monthEnd = moment(monthStart).endOf('month')
    const startDate = moment(monthStart).startOf('week')
    const endDate = moment(monthEnd).endOf('week')
    const dateFormat = 'D'
    const rows = []
    let days = []
    let day = startDate
    let formattedDate = ''
    while (day <= endDate) {
      for (let i = 0; i < 7; i++) {
        formattedDate = moment(day).format(dateFormat)
        const cloneDay = day
        const event = events.find(event =>
          moment(event.start).isSame(day, 'day')
        )
        days.push(
          <div
            className={classNames(styles.column, styles.cell)}
            key={nanoid(8)}
            onClick={() => onDateClick(moment(cloneDay).toDate())}
          >
            <div
              className={classNames(
                styles.card,
                !moment(day).isSame(monthStart, 'month')
                  ? styles.disabled
                  : moment(day).isSame(selectedDate, 'day')
                  ? styles.selected
                  : moment(day).isSame(currentDate, 'day')
                  ? styles.today
                  : event
                  ? styles.thereIsTasks
                  : ''
              )}
            >
              <span className={styles.bg}>{formattedDate}</span>
            </div>
          </div>
        )
        day = moment(day).add(1, 'd')
      }
      rows.push(
        <div className={styles.row} key={nanoid(8)}>
          {' '}
          {days}{' '}
        </div>
      )
      days = []
    }
    return <div className={styles.body}>{rows}</div>
  }

  const Square = (props: any) => {
    return (
      <i style={{ backgroundColor: props.color }} className={styles.square} />
    )
  }

  const mark = () => {
    return (
      <div className={styles.marks}>
        <span className={styles.mark}>
          <Square color="#aaaaaa" />
          Сегодня
        </span>
        <span className={styles.mark}>
          <Square color="#E64040" /> Есть задачи
        </span>
        <span className={styles.mark}>
          <Square color="#3AAF9F" /> Выбран
        </span>
      </div>
    )
  }

  const nextMonth = () => {
    setCurrentDate(
      moment(currentDate)
        .add(1, 'month')
        .toDate()
    )
  }

  const prevMonth = () => {
    setCurrentDate(
      moment(currentDate)
        .subtract(1, 'month')
        .toDate()
    )
  }

  const onDateClick = (day: Date) => {
    setSelectedDate(day)
  }

  return (
    <div className={styles.page}>
      <div className={styles.calendar}>
        <div>{header()}</div>
        <div>{days()}</div>
        <div>{cells()}</div>
        <div>{mark()}</div>
      </div>
    </div>
  )
}
export default CustomCalendar
