import React from 'react'
import styles from 'src/components/Feed.module.scss'
import FeedList from 'src/components/Feed/FeedList'

const Feed = () => {
  React.useEffect(() => {
    document.title = 'Напоминания | iTracker'
  }, [])
  return (
    <div className={styles.root}>
      <div className={styles.feedContainer}>
        <FeedList />
      </div>
    </div>
  )
}

export default Feed
