import React from 'react'
import { Redirect, BrowserRouter, Switch, Route } from 'react-router-dom'
import Auth from 'src/components/Auth'
import Feed from 'src/components/Feed'
import Profile from 'src/components/Profile'
import Calendar from 'src/components/Calendar/Calendar'

const ProtectedRoute = ({ isLoggedIn, ...props }: any) =>
  isLoggedIn ? <Route {...props} /> : <Redirect to="/auth/" />

const Routes = React.memo(() => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          {!!localStorage.getItem('logged') ? (
            <Redirect to="/feed/" />
          ) : (
            <Redirect to="/auth/" />
          )}
        </Route>
        <Route path="/auth/">
          <Auth login={true} />
        </Route>
        <Route path="/registration/">
          <Auth login={false} />
        </Route>
        <ProtectedRoute
          isLoggedIn={!!localStorage.getItem('logged')}
          path="/feed/"
        >
          <Feed />
        </ProtectedRoute>
        <ProtectedRoute
          isLoggedIn={!!localStorage.getItem('logged')}
          path="/calendar/"
        >
          <Calendar />
        </ProtectedRoute>
        <ProtectedRoute
          isLoggedIn={!!localStorage.getItem('logged')}
          path="/profile/"
        >
          <Profile />
        </ProtectedRoute>
      </Switch>
    </BrowserRouter>
  )
})

export default Routes
