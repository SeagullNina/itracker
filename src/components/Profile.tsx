import React from 'react'
import styles from 'src/components/Profile.module.scss'
import Navigation from 'src/components/Navigation'
import ProfileMain from 'src/components/Profile/ProfileMain'
import { IUser } from 'src/classes/models/IUser'

const Profile = () => {
  const [user, setUser] = React.useState<IUser>({} as IUser)
  React.useEffect(() => {
    document.title = 'Профиль | iTracker'
    setUser(JSON.parse(localStorage.getItem('user') as string))
  }, [])
  return (
    <div className={styles.root}>
      <div className={styles.profileContainer}>
        <Navigation />
        <ProfileMain user={user}/>
      </div>
    </div>
  )
}

export default Profile
