import React from 'react'
import styles from 'src/components/Auth.module.scss'
import AuthForm from 'src/components/Auth/AuthForm'
import RegistrationForm from 'src/components/Auth/RegistrationForm'
import Toggle from 'src/components/Auth/Toggle'

interface IProps {
  login: boolean
}

const Auth = (props: IProps) => {
  const { login } = props
  React.useEffect(() => {
    document.title = login ? 'Вход | iTracker' : 'Регистрация | iTracker'
  }, [login])
  return (
    <div className={styles.mainPage}>
      <div className={styles.iTracker}>
        <img alt="" className={styles.logo} src="../notes.png" />
        <img alt="" className={styles.tracker} src="../iTracker.png" />
        <div className={styles.description}>
          iTracker - это приложение для людей, желающих ввести в свою жизнь
          новые полезные привычки. Начните образ жизни, о котором давно мечтали,
          уже сегодня, вместе с трекером ежедневных заданий.
        </div>
      </div>
      <div className={styles.auth}>
        <Toggle login={login} />
        {login ? <AuthForm /> : <RegistrationForm />}
      </div>
    </div>
  )
}

Auth.defaultProps = {
  login: false,
}

export default Auth
