import React from 'react'
import styles from 'src/components/Header.module.scss'

const Header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.title}> HabiTracker</div>
    </div>
  )
}

export default Header
