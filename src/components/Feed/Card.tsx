import React from 'react'
import styles from './Card.module.scss'
import { IEvent } from 'src/classes/models/IEvent'
import EventsApi from '../../classes/services/api/EventsApi'

interface IProps {
  calendar: boolean
  item: IEvent
  setHabit: (item: IEvent) => void
  editable?: boolean
}

const Card = (props: IProps) => {
  const { calendar, item, setHabit, editable } = props
  const [edit, setEdit] = React.useState<boolean>(false)

  return (
    <div className={styles.box}>
      <div className={calendar ? styles.cardCalendar : styles.cardFeed}>
        <div className={styles.info}>
          <div className={calendar ? styles.headerCalendar : styles.headerFeed}>
            {item.title}
          </div>
          <div className={styles.content}>
            <span className={styles.strings}>
              {edit ? (
                <textarea
                  rows={10}
                  onChange={event => {
                    setHabit({ ...item, note: event.target.value })
                  }}
                  className={styles.input}
                  value={item.note}
                />
              ) : (
                <>
                  <b>Примечание:</b> {item.note}
                </>
              )}
            </span>
          </div>
        </div>
        {editable ? (
          edit ? (
            <div
              className={styles.edit}
              onClick={() =>
                EventsApi.editEvent(item._id, item.note, item.done).then(() =>
                  setEdit(false)
                )
              }
            >
              Сохранить
            </div>
          ) : (
            <div
              className={styles.edit}
              onClick={() =>
                  setEdit(true)}
            >
              Редактировать
            </div>
          )
        ) : item.done ? (
          <div
            onClick={() =>
              EventsApi.editEvent(item._id, item.note, false).then(() =>
                setHabit({ ...item, done: false })
              )
            }
            className={styles.done}
          >
            Отметить как невыполненное
          </div>
        ) : (
          <div
            onClick={() =>
              EventsApi.editEvent(item._id, item.note, true).then(() =>
                setHabit({ ...item, done: true })
              )
            }
            className={styles.done}
          >
            Отметить как выполненное
          </div>
        )}
      </div>
    </div>
  )
}

export default Card
