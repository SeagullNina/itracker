import React from 'react'
import Navigation from 'src/components/Navigation'
import Card from './Card'
import styles from './FeedList.module.scss'
import nanoid from 'nanoid'
import { IEvent } from 'src/classes/models/IEvent'
import EventsApi from 'src/classes/services/api/EventsApi'
import { ReactComponent as CompletedIcon } from 'src/assets/icons/completed.svg'
import { ReactComponent as ClockIcon } from 'src/assets/icons/clock.svg'
import moment from 'moment'

const FeedList = () => {
  const [habits, setHabits] = React.useState<IEvent[]>([] as IEvent[])
  const [done, setDone] = React.useState<boolean>(false)
  const setHabit = (item: IEvent) => {
    const newHabits = habits.filter((i: IEvent) => i._id !== item._id)
    newHabits.push(item)
    setHabits(newHabits)
  }

  React.useEffect(() => {
    const userId = JSON.parse(localStorage.getItem('user') as string)._id
    EventsApi.fetchEvents(userId).then(result => setHabits(result.items))
  }, [])

  return (
    <div className={styles.page}>
      <Navigation />
      <div className={styles.content}>
        <div className={styles.tab}>
          <div
            className={done ? styles.activeBlock : styles.block}
            onClick={() => {
              setDone(true)
            }}
          >
            <ClockIcon className={styles.icon} />
            <span>Ожидают выполнения</span>
          </div>
          <div
            className={!done ? styles.activeBlock : styles.block}
            onClick={() => {
              setDone(false)
            }}
          >
            <CompletedIcon className={styles.icon} />
            <span>Выполненные</span>
          </div>
          <div className={styles.date}>
            <b>{moment().format('Do MMMM YYYY, dddd')}</b>
          </div>
        </div>
        <div className={styles.cards}>
          {habits
            .filter((item: IEvent) => item.done === !done)
            .map((item: IEvent) => (
              <Card
                calendar={false}
                setHabit={(i: IEvent) => setHabit(i)}
                item={item}
                key={nanoid(8)}
              />
            ))}
        </div>
      </div>
    </div>
  )
}

export default FeedList
