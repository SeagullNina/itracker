import React from 'react'
import styles from './RegistrationForm.module.scss'
import UserApi from 'src/classes/services/api/UsersApi'
import * as Validator from 'src/classes/validators/EmailValidator'
import {IUser} from "../../classes/models/IUser";

interface IProps {}

const RegistrationForm = (props: IProps) => {
  const [loading, setLoading] = React.useState<boolean>(false)
  const [errorLogin, setErrorLogin] = React.useState(false)
  const [errorPassword, setErrorPassword] = React.useState(false)
  const [errorEmpty, setErrorEmpty] = React.useState(false)
  const [name, setName] = React.useState<string>('')
  const [email, setEmail] = React.useState<string>('')
  const [password, setPassword] = React.useState<string>('')
  const [yetPassword, setYetPassword] = React.useState<string>('')

  return (
    <div className={styles.page}>
      <div className={styles.header}>Создать аккаунт</div>
      <div className={styles.form}>
        <span>Имя</span>
        <input
          className={styles.input}
          type="text"
          placeholder={'Введите ваше имя'}
          onChange={event => setName(event.target.value)}
        />
        <span>Email</span>
        <input
          className={styles.input}
          type="text"
          placeholder={'Введите ваш электронный адрес'}
          onChange={event => setEmail(event.target.value)}
          onBlur={(event)=> {if (!Validator.emailValidation(event.target.value)) setErrorLogin(true);
          else setErrorLogin(false)}}
        />
        <span className={errorLogin ? styles.error: styles.none}>
            Неправильный формат ввода.
          </span>
        <span>Пароль</span>
        <input
          className={styles.input}
          type="password"
          placeholder={'Введите ваш пароль'}
          onChange={event => setPassword(event.target.value)}
        />
        <span>Повторите пароль</span>
        <input
          className={styles.input}
          type="password"
          placeholder={'Введите ваш пароль'}
          onChange={event => setYetPassword(event.target.value)}
        />
        <span className={errorPassword ? styles.error:styles.none }>
          Пароли не совпадают.
        </span>
        <span className={errorEmpty ? styles.error:styles.none }>
          Пожалуйста, заполните все поля.
        </span>
        <input
          disabled={loading}
          className={styles.button}
          type="button"
          value="Создать"
          onClick={() => {
              if (name && email && password === yetPassword) {
                setErrorPassword(false);
                setErrorEmpty(false);
                setLoading(true)
                UserApi.registration({ email, password, name } as IUser).then(res => {
                  setLoading(false)
                  window.location.href = '/feed/';
                  localStorage.setItem('logged', '1');
                  localStorage.setItem('user', JSON.stringify(res.data))
                }).catch(() => {
                  setErrorEmpty(true);
                  setLoading(false)
                })
              }
              else
                if (password !== yetPassword){
                  setErrorPassword(true);
                  return
                }
                else
                  if (!name || !email || !password || !yetPassword){
                    setErrorPassword(false);
                    setErrorEmpty(true);
                    return
                  }
            }
          }
        />
      </div>
    </div>
  )
}

export default RegistrationForm
