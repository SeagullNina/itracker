import React from 'react'
import styles from 'src/components/Auth.module.scss'
import { Link } from 'react-router-dom'
import classnames from 'classnames'
import toggle from 'src/components/Auth/Toggle.module.scss'

interface IProps {
  login: boolean
}

const Toggle = (props: IProps) => {
  return (
    <div className={styles.toggle}>
      <Link to="/registration/">
        <input
          id={!props.login ? 'toggle-on' : 'toggle-off'}
          className={classnames(toggle.toggle, toggle.toggleLeft)}
          name="toggle"
          value={`${!props.login}`}
          type="radio"
          checked={!props.login}
        />
        <label
          htmlFor={!props.login ? 'toggle-on' : 'toggle-off'}
          className={toggle.btnCreate}
        >
          Создать
        </label>
      </Link>
      <Link to="/auth/">
        <input
          id={props.login ? 'toggle-on' : 'toggle-off'}
          className={classnames(toggle.toggle, toggle.toggleRight)}
          name="toggle"
          value={`${props.login}`}
          type="radio"
          checked={props.login}
        />
        <label
          htmlFor={props.login ? 'toggle-on' : 'toggle-off'}
          className={toggle.btnEnter}
        >
          Войти
        </label>
      </Link>
    </div>
  )
}

export default Toggle
