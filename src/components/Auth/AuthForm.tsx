import React from 'react'
import styles from './AuthForm.module.scss'
import UserApi from 'src/classes/services/api/UsersApi'
import * as Validator from 'src/classes/validators/EmailValidator'

interface IProps {}

const AuthForm = (props: IProps) => {
  const [loading, setLoading] = React.useState<boolean>(false)
  const [error, setError] = React.useState(false)
  const [errorLogin, setErrorLogin] = React.useState(false)
  const [remember, setRemember] = React.useState<boolean>(false)
  const [email, setEmail] = React.useState<string>('')
  const [password, setPassword] = React.useState<string>('')
  return (
    <div className={styles.page}>
      <div className={styles.header}>Войти в iTracker</div>
      <div className={styles.form}>
        <span>Email</span>
        <input
          id="email"
          className={styles.input}
          type="text"
          onChange={event => setEmail(event.target.value)}
          placeholder={'Введите ваш электронный адрес'}
          onBlur={event => {
            if (!Validator.emailValidation(event.target.value))
              setErrorLogin(true)
            else setErrorLogin(false)
          }}
        />
        <span className={errorLogin ? styles.error : styles.none}>
          Неправильный формат ввода.
        </span>
        <span>Пароль</span>
        <input
          id="password"
          className={styles.input}
          type="password"
          onChange={event => setPassword(event.target.value)}
          placeholder={'Введите ваш пароль'}
        />
        <div
          className={styles.checkbox}
          onClick={() => setRemember(prevState => !prevState)}
        >
          <span className={error ? styles.error : styles.none}>
            Неправильно введен логин или пароль.
          </span>
          <br />
          <input type="checkbox" checked={remember} />
          <span className={styles.box}>Запомнить меня</span>
        </div>
        <input
          disabled={loading}
          className={styles.button}
          type="button"
          value="Войти"
          onClick={() => {
            setLoading(true)
            UserApi.auth(email, password)
              .then(result => {
                setLoading(false)
                setError(false)
                window.location.href = '/feed/'
                localStorage.setItem('logged', '1')
                localStorage.setItem('user', JSON.stringify(result.data))
              })
              .catch(() => {
                setLoading(false)
                setError(true)
              })
          }}
        />
      </div>
    </div>
  )
}

export default AuthForm
