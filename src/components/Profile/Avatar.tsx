import styles from './Avatar.module.scss'
import Gravatar from 'react-gravatar'
import React from 'react'

interface IProps {
  email: string
}

const Avatar = (props: IProps) => {
  const { email } = props
  const [download, setDownload] = React.useState(false)

  return (
    <>
      {download ? (
        <div
          className={styles.download}
          onMouseLeave={() => {
            setDownload(false)
          }}
          onMouseEnter={() => {
            setDownload(true)
          }}
          onClick={() => {
            window.open(
              'https://ru.gravatar.com/gravatars/new',
              'Change avatar',
              'location=1,status=1,scrollbars=1,with=500px,height=500px'
            )
          }}
        >
          <img
            src="../download.png"
            alt="Нажмите сюда, чтобы загрузить новое изображение"
            className={styles.downloadIcon}
          />
          <span className={styles.descr}>
            Нажмите сюда, чтобы загрузить новое изображение
          </span>
        </div>
      ) : (
        <></>
      )}
      <Gravatar
        email={email}
        default="wavatar"
        className={styles.avatar}
        size={270}
        onMouseEnter={() => {
          setDownload(true)
        }}
        onMouseLeave={() => {
          setDownload(false)
        }}
      />
    </>
  )
}

export default Avatar
