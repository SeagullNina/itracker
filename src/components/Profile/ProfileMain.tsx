import React from 'react'
import { IUser } from 'src/classes/models/IUser'
import styles from './ProfileMain.module.scss'
import { NavLink } from 'react-router-dom'
import { ReactComponent as LogoutIcon } from 'src/assets/icons/logout.svg'
import Modal from 'src/components/shared/Modal'
import Tasks from 'src/components/shared/Tasks'
import Avatar from 'src/components/Profile/Avatar'
import EventsApi from '../../classes/services/api/EventsApi'
import UsersApi from '../../classes/services/api/UsersApi'
import moment from 'moment'
import { getAchievements, IAchievement } from '../shared/Achievements'
import {IEvent} from "../../classes/models/IEvent";

interface IProps {
  user: IUser
}

const ProfileMain = (props: IProps) => {
  const { user } = props
  const [edit, setEdit] = React.useState<boolean>(false)
  const [name, setName] = React.useState<string>('')
  const [email, setEmail] = React.useState<string>('')
  const [date, setDate] = React.useState<string>('')
  const [ach, setAch] = React.useState<IAchievement[]>([])
  const [lookMore, setLookMore] = React.useState<boolean>(false)

  const [tasks, setTasks] = React.useState()

  React.useEffect(() => {
    const userId = JSON.parse(localStorage.getItem('user') as string)._id
    EventsApi.fetchEvents(userId).then(result => setTasks(result.items))
  }, [])

  React.useEffect(() => {
    setName(user.name)
    setEmail(user.email)
    setDate(moment(user.dateOfReg).format('DD.MM.YYYY'))
    user.achievements && setAch(getAchievements(user.achievements))
  }, [props.user])

  return (
    <div className={styles.page}>
      <div className={styles.left}>
        <Avatar email={user.email} />
        <div className={styles.changeUser}>
          <NavLink
            to="/auth"
            onClick={() => localStorage.setItem('logged', '')}
            className={styles.link}
          >
            <LogoutIcon className={styles.icon} />
            <span className={styles.menuName}>Сменить пользователя</span>
          </NavLink>
        </div>
      </div>
      <div className={styles.middle}>
        <div className={styles.about}>
          <div className={styles.header}>Немного о себе</div>
          <div className={styles.content}>
            <span className={styles.infoItem}>
              <b>Имя: </b>
              {edit ? (
                <input
                  type="text"
                  value={name}
                  onChange={event => {
                    setName(event.target.value)
                  }}
                  className={styles.input}
                />
              ) : (
                name
              )}
            </span>
            <span className={styles.infoItem}>
              <b>Email: </b>
              {edit ? (
                <input
                  type="text"
                  value={email}
                  onChange={event => {
                    setEmail(event.target.value)
                  }}
                  className={styles.input}
                />
              ) : (
                email
              )}
            </span>
            <span className={styles.infoItem}>
              <b>Дата регистрации: </b>
              {date}
            </span>
          </div>
          {edit ? (
            <div
              className={styles.edit}
              onClick={() => {
                UsersApi.editUser(user._id, name, email).then(() =>
                  setEdit(false)
                )
                localStorage.setItem(
                  'user',
                  JSON.stringify({ ...user, name, email })
                )
              }}
            >
              Сохранить
            </div>
          ) : (
            <div
              className={styles.edit}
              onClick={() => {
                setEdit(true)
              }}
            >
              Редактировать
            </div>
          )}
        </div>
        <div className={styles.about}>
          <div className={styles.header}>Мои достижения</div>
          <div className={styles.content}>
            {ach.slice(0, 2).map((ach: any, index) => (
              <div className={styles.achieve} key={index}>
                <img alt="" className={styles.achImage} src={ach.src} />
                <span>
                  <b>{ach.title}</b>
                  {ach.description}
                </span>
              </div>
            ))}
          </div>
          <div
            className={lookMore ? styles.none : styles.edit}
            onClick={() => {
              setLookMore(true)
            }}
          >
            Смотреть больше
          </div>
        </div>
      </div>
      <div className={styles.myTasks}>
        <div className={styles.header}>Мои задания</div>
        {tasks && <Tasks profile={true} events={tasks} setEvents={setTasks} />}
      </div>
      <Modal
        label="Мои достижения"
        onClose={() => setLookMore(false)}
        opened={lookMore}
      >
        <>
          {ach.map((ach: any, index: number) => (
            <div className={styles.achieve} key={index}>
              <img alt="" className={styles.achImage} src={ach.src} />
              <span>
                <b>{ach.title}</b>
                {ach.description}
              </span>
            </div>
          ))}
        </>
      </Modal>
    </div>
  )
}
export default ProfileMain
