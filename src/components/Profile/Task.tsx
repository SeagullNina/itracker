import React from 'react'
import styles from './Task.module.scss'
import { ReactComponent as CloseIcon } from 'src/assets/icons/cross.svg'
import { IEvent } from 'src/classes/models/IEvent'
import EventsApi from "../../classes/services/api/EventsApi";

interface IProps {
  active: boolean
  calendar: boolean
  onClick?: (task: any) => void
  deleteTask: (task: string) => void
  task: IEvent
}

const Task = (props: IProps) => {
  const { active, calendar, onClick, deleteTask, task } = props
  const [close, setClose] = React.useState(true)

  return (
    <div
      className={calendar ?  (active ? styles.taskActive : styles.taskCalendar) : styles.taskProfile}
      onMouseLeave={() => setClose(true)}
      onMouseEnter={() => setClose(false)}
      onClick={() => {return onClick && onClick(task)}}
    >
      {task.title}
      <CloseIcon
        className={calendar ? (close ? styles.close : styles.closeCalendarActive) : (close ? styles.close : styles.closeProfileActive)}
        onClick={() => {
          deleteTask(task._id)
          EventsApi.deleteEvent(task._id)
        }}
      />
    </div>
  )
}

export default Task
