import React from 'react'
import styles from 'src/components/Navigation.module.scss'
import { ReactComponent as EditIcon } from 'src/assets/icons/edit.svg'
import { ReactComponent as CalendarIcon } from 'src/assets/icons/calendar.svg'
import { ReactComponent as UserIcon } from 'src/assets/icons/user.svg'
import { NavLink } from 'react-router-dom'

const Navigation = () => {
  return (
    <div className={styles.mainPage}>
      <div className={styles.navigation}>
        <div className={styles.logoBlock}>
          <img alt="" className={styles.logo} src="../mainLogo.png" />
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            exact
            to="/feed/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <EditIcon className={styles.icon} />
            <span className={styles.menuName}>Мои напоминания</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/calendar/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <CalendarIcon className={styles.icon} />
            <span className={styles.menuName}>Мой календарь</span>
          </NavLink>
        </div>
        <div className={styles.menuBlock}>
          <NavLink
            to="/profile/"
            className={styles.link}
            activeClassName={styles.selected}
          >
            <UserIcon className={styles.icon} />
            <span className={styles.menuName}>Мой профиль</span>
          </NavLink>
        </div>
      </div>
    </div>
  )
}

export default Navigation
