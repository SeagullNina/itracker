import React, { useState } from 'react'
import Navigation from 'src/components/Navigation'
import styles from 'src/components/Calendar/Calendar.module.scss'
import { IEvent } from 'src/classes/models/IEvent'
import EventsApi from 'src/classes/services/api/EventsApi'
import CustomCalendar from 'src/components/shared/CustomCalendar'
import moment from 'moment'
import Tasks from '../shared/Tasks'
import Card from '../Feed/Card'

const Calendar = () => {
  const [events, setEvents] = React.useState<IEvent[]>([] as IEvent[])
  const [event, setEvent] = useState<IEvent>()
  const [currentDate, setCurrentDate] = useState<Date>(new Date())
  const [selectedDate, setSelectedDate] = useState<Date>(new Date())
  const [tasks, setTasks] = React.useState()

  React.useEffect(() => {
    document.title = 'Календарь | iTracker'
    const userId = JSON.parse(localStorage.getItem('user') as string)._id
    EventsApi.fetchEvents(userId).then(results => setEvents(results.items))
  }, [])

  React.useEffect(() => {
    setTasks(
      events.filter((task: IEvent) =>
        moment(task.start).isSame(selectedDate, 'day')
      )
    )
    setEvent(
      events.find(task => moment(task.start).isSame(selectedDate, 'day'))
    )
  }, [events, selectedDate])

  return (
    <div className={styles.root}>
      <div className={styles.calendarContainer}>
        <Navigation />
        <CustomCalendar
          setSelectedDate={setSelectedDate}
          setCurrentDate={setCurrentDate}
          width={500}
          height={500}
          events={events}
          currentDate={currentDate}
          selectedDate={selectedDate}
        />
        <div className={styles.info}>
          <div className={styles.now}>
            <b>{moment(selectedDate).format('Do MMMM YYYY, dddd')}</b>
          </div>
          <div className={styles.right}>
            <div className={styles.tasks}>
              <Tasks
                profile={false}
                events={events}
                tasks={tasks}
                setEvents={setEvents}
                setTask={setEvent}
                currentDate={selectedDate}
              />
            </div>
            <div className={styles.cards}>
              {event && (
                <Card
                  calendar={true}
                  item={event}
                  setHabit={e => setEvent(e)}
                  editable
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Calendar
