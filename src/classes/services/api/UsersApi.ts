import { IUser } from 'src/classes/models/IUser'
import axios, { AxiosInstance } from 'axios'
import settings from 'src/config/settings'

class UserApi {
  private http: AxiosInstance = axios.create({
    baseURL: settings.api,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json;charset=utf-8',
    },
  })

  public registration = async (user: IUser) => {
    return await this.http.post('/registration', { ...user })
  }

  public auth = async (email: string, password: string) => {
    return await this.http.post('/auth', { email, password })
  }

  public editUser = async (_id: string, name: string, email: string) => {
    return await this.http.put('/profile', { _id, name, email })
  }
}

export default new UserApi()
