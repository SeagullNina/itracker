import { IFetchResult } from 'src/classes/models/IFetchResult'
import { IEvent } from 'src/classes/models/IEvent'
import axios, { AxiosInstance } from 'axios'
import settings from '../../../config/settings'

class EventsApi {
  private http: AxiosInstance = axios.create({
    baseURL: settings.api,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json;charset=utf-8',
    },
  })

  public editEvent = async (_id: string, note: string, done: boolean) => {
    return await this.http.put('/event', { _id, note, done })
  }

  public deleteEvent = async (_id: string) => {
    return await this.http.delete(`/event?_id=${_id}`)
  }

  public createEvent = async (event: Partial<IEvent>) => {
    return await this.http.post('/event', event)
  }

  public fetchEvents = async (
    userId: string
  ): Promise<IFetchResult<IEvent>> => {
    const fetchedEvents = await this.http.get(`/event?userId=${userId}`)
    return {
      items: fetchedEvents.data,
      total: fetchedEvents.data.length,
    }
  }
}

export default new EventsApi()
