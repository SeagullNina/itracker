export enum IDuration {
  day,
  week,
  month,
  threeMonths,
}
