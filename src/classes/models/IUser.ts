import { IForm } from 'src/classes/models/IForm'

export interface IUser extends IForm {
  _id: string
  achievements: number[]
  email: string
  password: string
  name: string
  dateOfReg: string
}
