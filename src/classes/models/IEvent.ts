import { IForm } from 'src/classes/models/IForm'
import { IDuration } from './IDuration'

export interface IEvent extends IForm {
  _id: string
  start: string
  end: string
  title: string
  note: string
  frequency?: any
  duration?: IDuration
  done: boolean
}
